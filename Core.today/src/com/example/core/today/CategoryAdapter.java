package com.example.core.today;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter{
	public CategoryAdapter(Context context, int resource) {
		super(context, resource);
		
	}

	Context context;
	

	
	public int getCount(){
		return 24;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		
		if(row==null){
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.grid_row, parent, false);
            
            TextView textViewTitle = (TextView) row.findViewById(R.id.title_text);
            if(position%2==0)
            {
                    textViewTitle.setText("Facebook");
                    
            }
            else
            {
                    textViewTitle.setText("Twitter");
            }
		}
		
	
		return row;
	}
}
