package com.example.core.today;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SearchNews extends Activity{
	private ListView mListView;
	private SearchNewsAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_news);
		mAdapter = new SearchNewsAdapter();
		mListView = (ListView)findViewById(R.id.search_listview);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(onClickListItem);
		
	}
	
	private OnItemClickListener onClickListItem = new OnItemClickListener(){
		
		@Override
		public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
			
		}
		
	};
	
}
