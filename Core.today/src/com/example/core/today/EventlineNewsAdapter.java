package com.example.core.today;

import java.util.ArrayList;
import java.util.TreeSet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class EventlineNewsAdapter extends BaseAdapter{

	
	private static final int TYPE_ITEM = 0;
	private static final int TYPE_SEPARATOR = 1;
	
	
	private ArrayList<String> mList;
	private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();
	
	public EventlineNewsAdapter(){
		mList = new ArrayList<String>();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public void addItem(final String item){
		mList.add(item);
		notifyDataSetChanged();
	}
	
	public void addSectionHeaderItem(final String item) {
		mList.add(item);
		sectionHeader.add(mList.size() - 1);
		notifyDataSetChanged();
	}
	
	@Override
	public int getItemViewType(int position) {
		return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
	}
	
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		int rowType = getItemViewType(position);
		final int pos = position;
		final Context context = parent.getContext();
		
		if(convertView == null){
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			switch(rowType){
			case TYPE_ITEM:
				convertView = inflater.inflate(R.layout.eventline_news_item, parent, false);
				holder.textView = (TextView)convertView.findViewById(R.id.eventline_title);
				break;
			case TYPE_SEPARATOR:
				convertView = inflater.inflate(R.layout.eventline_news_item2, parent, false);
				holder.textView = (TextView)convertView.findViewById(R.id.textSeparator);
			}
			
			
			holder.textView.setText(mList.get(position));
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
				}
			});
		}
		return convertView;
	}
	
	public static class ViewHolder {
		public TextView textView;
	}

}
