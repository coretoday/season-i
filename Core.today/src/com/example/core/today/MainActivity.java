package com.example.core.today;

import viewpagerindicator.TabPageIndicator;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshListView;


public class MainActivity extends FragmentActivity implements OnClickListener  {
	private static final String[] CONTENT = new String[] { "Recent", "Artists", "Albums" };

	boolean isPageOpen = false;
	Animation translateLeftAnim;
	Animation translateRightAnim;
	LinearLayout leftmenu;
	LinearLayout frontpage;
	LinearLayout empty;
	FrameLayout main;
	ImageView menu_btn;
	Button login_btn;
	static final int LOGIN = 0;
	static final int PROFILE = 1;
	static final int MENU_SET_MODE = 2;
	static final int MENU_DEMO = 3;

	private PullToRefreshListView mPullRefreshListView;
	

	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_ptr_list_in_vp);	
		
		////////////////////////////////////////////////////////////////////////////////
		FragmentPagerAdapter adapter = new ListViewPagerAdapter(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.vp_list);
		mViewPager.setAdapter(adapter);
		TabPageIndicator indicator = (TabPageIndicator)findViewById(R.id.indicator);
	    indicator.setViewPager(mViewPager);
	    ////////////////////////////////////////////////////////////////////////////////activate fragment
	    
	    
	    menu_btn = (ImageView)findViewById(R.id.menu_btn);  
	    empty=(LinearLayout) findViewById(R.id.empty);
	    frontpage=(LinearLayout)findViewById(R.id.abc);
	    leftmenu=(LinearLayout)findViewById(R.id.leftmenu);
	    login_btn= (Button)findViewById(R.id.login_total);
	    
	    
	    leftmenu.setOnClickListener(this);
	    frontpage.setOnClickListener(this);
	    empty.setOnClickListener(this);  
	    menu_btn.setOnClickListener(this);
	    login_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent login_intent = null;
				login_intent = new Intent(MainActivity.this, Login.class);
				startActivity(login_intent);
				
			}
		});
	    
	    /////////////////////////////////////////////////////////////////////////////////////
	    translateLeftAnim = AnimationUtils.loadAnimation(this, R.anim.translate_left);
	    translateRightAnim = AnimationUtils.loadAnimation(this, R.anim.translate_right);
	    SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
	    translateLeftAnim.setAnimationListener(animListener);
	    translateRightAnim.setAnimationListener(animListener);
	    ////////////////////////////////////////////////////////////////////////////////leftmenu Animation
	    
	   

	    TextView timeline_btn = (TextView)findViewById(R.id.timeline);
	 
	    timeline_btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = null;
				intent = new Intent(MainActivity.this, TimeLine.class);
				startActivity(intent);
				
			}
		});
	    

	}
    private class SlidingPageAnimationListener implements AnimationListener {
    	/**
    	 * 좌측 메뉴 애니메이션이 끝날 때 호출되는 메소드
    	 */
		public void onAnimationEnd(Animation animation) {
			if(isPageOpen)
			{
				
				frontpage.setClickable(true);
			}
			else
			{
				
				frontpage.setClickable(false);
			}
		}

		public void onAnimationRepeat(Animation animation) {
			

		}

		public void onAnimationStart(Animation animation) {
			if(isPageOpen)
			{
				leftmenu.setVisibility(View.INVISIBLE);
				empty.setVisibility(View.INVISIBLE);
				isPageOpen=false;
			}
			else
			{
				isPageOpen=true;
				empty.setVisibility(View.VISIBLE);
			}
		}

    }

	private class ListViewPagerAdapter extends FragmentPagerAdapter {

		public ListViewPagerAdapter(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}
		/*
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
		*/

        @Override
        public Fragment getItem(int position) {
            return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }



	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, LOGIN, 0, "Login to Core.2day");
		menu.add(0, PROFILE, 0, "My Profile");
		
		return super.onCreateOptionsMenu(menu);
	}

	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;

		menu.add("Item 1");
		menu.add("Item 2");
		menu.add("Item 3");
		menu.add("Item 4");

		super.onCreateContextMenu(menu, v, menuInfo);
	}


	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case LOGIN:
				Intent login_intent = null;
				login_intent = new Intent(MainActivity.this, Login.class);
				startActivity(login_intent);
				finish();
			    break;
			case PROFILE:
				
				break;
			case MENU_SET_MODE:
				mPullRefreshListView.setMode(mPullRefreshListView.getMode() == Mode.BOTH ? Mode.PULL_FROM_START
						: Mode.BOTH);
				break;
			case MENU_DEMO:
				mPullRefreshListView.demo();
				break;
		}
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		
		////////////////////////////////////////////////////////////
		if(v.getId()==R.id.menu_btn)
		{
			leftmenu.setVisibility(View.VISIBLE);
			leftmenu.startAnimation(translateRightAnim);
		}
		else if(isPageOpen)
		{
			if(v.getId()!=R.id.leftmenu)
			{
				leftmenu.startAnimation(translateLeftAnim);
			}
		}
		///////////////////////////////////////////////////////////leftmenu animation
		
		
	}
}
